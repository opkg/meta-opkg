require recipes-devtools/opkg/opkg.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=94d55d512a9ba36caa9b7df079bae19f \
                    file://src/opkg.c;beginline=2;endline=21;md5=90435a519c6ea69ef22e4a88bcc52fa0"

OPKG_GIT_REPO ?= "git://git.yoctoproject.org/opkg"
OPKG_GIT_BRANCH ?= "master"
OPKG_GIT_RELEASE ?= "0.3.0~SNAPSHOT"

SRC_URI = "${OPKG_GIT_REPO};branch=${OPKG_GIT_BRANCH} \
           file://opkg-configure.service \
"

DEPENDS += "libarchive"

SRCREV = "${AUTOREV}"
PV = "${OPKG_GIT_RELEASE}+git${SRCPV}"

S = "${WORKDIR}/git"
