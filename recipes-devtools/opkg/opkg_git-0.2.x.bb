require recipes-devtools/opkg/opkg.inc

OPKG_GIT_REPO ?= "git://git.yoctoproject.org/opkg"
OPKG_GIT_BRANCH ?= "opkg-0.2.x"
OPKG_GIT_RELEASE ?= "0.2.4~SNAPSHOT"

SRC_URI = "${OPKG_GIT_REPO};branch=${OPKG_GIT_BRANCH} \
           file://0001-opkg-0.2.x-no-install-recommends.patch \
           file://0002-opkg-0.2.x-add-exclude.patch \
           file://opkg-configure.service \
"

SRCREV = "${AUTOREV}"
PV = "${OPKG_GIT_RELEASE}+git${SRCPV}"

S = "${WORKDIR}/git"
